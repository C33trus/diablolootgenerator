﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class LootGenerator : MonoBehaviour {
    DataBase db;
    bool isFighting = false;

    void Start() {
        db = DBHelper.Load();
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.G) && !isFighting) {
            StartCoroutine(Generate());
        }
    }

    IEnumerator Generate() {
        isFighting = true;
        // slaying monster
        MonStat slayedMon = db.monstats.Values.ToArray()[Random.Range(0,db.monEntries)];
        Debug.Log("Fighting "+slayedMon.Class+" ...");

        // get item
        TreasureClass tc = db.tcs[slayedMon.TreasureClass];
        string itemName = "";
        while (true) {
            itemName = tc.Items[Random.Range(0, 3)];
            if (itemName.Contains("tc:"))
                tc = db.tcs[itemName];
            else break;
        }

        yield return new WaitForSeconds(0.5f);
        Debug.Log("You have slain " + slayedMon.Class + "!");
        // computing base stats
        EquipmentType eqType = EquipmentType.Armor;
        int[] baseStats=null;
        switch (tc.Name.Substring(3, 4)) {
            case "armo": 
                eqType = EquipmentType.Armor;
                baseStats = new int[1] { Random.Range(db.armors[itemName].MinAc, db.armors[itemName].MaxAc + 1) };
                break;
            case "weap":
                eqType = EquipmentType.Weapon;
                baseStats = new int[] { db.weapons[itemName].MinDam, db.weapons[itemName].MaxDam };
                break;
        }

        // generating affixes
        NonGeneratedPrefix p=null;
        NonGeneratedSuffix s=null;
        if (Random.value < 0.5f) p = db.prefixes[Random.Range(0, db.prefixEntries)];
        if (Random.value < 0.5f) s = db.suffixes[Random.Range(0, db.suffixEntries)];
        Prefix prefix=null;
        Suffix suffix=null;
        if (p != null) prefix = new Prefix(p);
        if (s != null) suffix = new Suffix(s);

        // creating item
        Item item = new Item(eqType, itemName, baseStats, prefix, suffix);

        Debug.Log(slayedMon.Class + " dropped:");
        Debug.Log("=====\n"+item.ToString()+"\n=====");
        isFighting = false;
        yield break;
    }

}
