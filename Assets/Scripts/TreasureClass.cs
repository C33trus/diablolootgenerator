﻿using UnityEngine;
using System.Collections;

public class TreasureClass {
    public string Name;
    public string[] Items;

    public TreasureClass(string name, string item1, string item2, string item3) {
        this.Name = name;
        this.Items = new string[] { item1, item2, item3 };
    }
}
