﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class MonStat {
    public string Class;
    public string Type;
    public int Level;
    public string TreasureClass;

    public MonStat(string Class, string Type, int Level, string TreasureClass) {
        this.Class = Class;
        this.Type = Type;
        this.Level = Level;
        this.TreasureClass = TreasureClass;
    }
}
