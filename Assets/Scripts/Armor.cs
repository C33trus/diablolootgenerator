﻿using UnityEngine;
using System.Collections;

public class Armor {
    public string Name;
    public int MinAc;
    public int MaxAc;

    public Armor(string name, int minAc, int maxAc) {
        this.Name = name;
        this.MinAc = minAc;
        this.MaxAc = maxAc;
    }
}
