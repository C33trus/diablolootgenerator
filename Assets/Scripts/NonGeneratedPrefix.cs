﻿using UnityEngine;
using System.Collections;

public class NonGeneratedPrefix :Affix {
    public NonGeneratedPrefix(string name, string code, int min, int max) : base(name, code, min, max) { }
}
