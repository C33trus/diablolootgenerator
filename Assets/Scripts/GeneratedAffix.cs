﻿using UnityEngine;
using System.Collections;

public abstract class GeneratedAffix  {
    public string Name;
    public string Code;
    public int Value;
    public bool isProcValue;

    public GeneratedAffix(Affix a) {
        this.Name = a.Name.Replace("_"," ");
        this.Code = a.Code.Replace("_"," ");
        if (this.Code.Contains("%")) {
            isProcValue = true;
            this.Code = this.Code.Replace(" %", "");
        }
        this.Value = Random.Range(a.Min,a.Max+1);
    }
}
