﻿using UnityEngine;
using System.Collections;

public class NonGeneratedSuffix :Affix {
    public NonGeneratedSuffix(string name, string code, int min, int max) : base(name, code, min, max) { }
}
