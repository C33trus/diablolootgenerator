﻿using UnityEngine;
using System.Collections;

public class Weapon  {
    public string Name;
    public int MinDam;
    public int MaxDam;
    public string Class;

    public Weapon(string name, int mindam, int maxdam, string wpnClass) {
        this.Name = name;
        this.MinDam = mindam;
        this.MaxDam = maxdam;
        this.Class = wpnClass;
    }
}
