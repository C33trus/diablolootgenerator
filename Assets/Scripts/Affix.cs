﻿using UnityEngine;
using System.Collections;

public abstract class Affix {
    public string Name;
    public string Code;
    public int Min;
    public int Max;

    public Affix(string name, string code, int min, int max) {
        this.Name = name;
        this.Code = code;
        this.Min = min;
        this.Max = max;
    }
}
