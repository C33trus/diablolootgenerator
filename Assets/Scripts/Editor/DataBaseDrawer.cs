﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer(typeof(DataBase))]
public class DataBaseDrawer : PropertyDrawer {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        EditorGUI.BeginProperty(position, label, property);

        SerializedProperty monent = property.FindPropertyRelative("monEntries");
        EditorGUI.LabelField(position, monent.intValue.ToString());

        EditorGUI.EndProperty();
    }
}
