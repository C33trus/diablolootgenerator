﻿using UnityEngine;
using System.Collections;

public enum EquipmentType { Armor, Weapon }
public interface IItem {

    string Name { get; }
    EquipmentType Type { get; }
    int[] BaseStats { get; }
    Prefix prefix { get; }
    Suffix suffix { get; }
}
