﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

[Serializable]
public class DataBase {
    public Dictionary<string, Armor> armors;
    public Dictionary<string, Weapon> weapons;
    public Dictionary<string,MonStat> monstats;
    public Dictionary<string, TreasureClass> tcs;
    public List<NonGeneratedPrefix> prefixes;
    public List<NonGeneratedSuffix> suffixes;

    public int monEntries = 0;
    public int prefixEntries = 0;
    public int suffixEntries = 0;
    

    internal DataBase() {
        armors = new Dictionary<string, Armor>();
        weapons = new Dictionary<string, Weapon>();
        monstats = new Dictionary<string, MonStat>();
        tcs = new Dictionary<string, TreasureClass>();
        prefixes = new List<NonGeneratedPrefix>();
        suffixes = new List<NonGeneratedSuffix>();
    }
}

public static class DBHelper {
    const string PATH = "/Data/";
    const string ARM_PATH = "armor.txt";
    const string WPN_PATH = "weapons.txt";
    const string PRF_PATH = "MagicPrefix.txt";
    const string SUF_PATH = "MagicSuffix.txt";
    const string MON_PATH = "monstats.txt";
    const string TCE_PATH = "TreasureClassEx.txt";

    public static string oneHandedWpn = "1hs, 1ht, ht1";

    public static DataBase Load() {
        DataBase db = new DataBase();
        string path = Application.dataPath + PATH;
        
        string line;

        #region Armor
        using (StreamReader reader = new StreamReader(path + ARM_PATH)) {
            do {
                line = reader.ReadLine();

                if (line != null) {
                    if (!line.Contains("minac")) {
                        string[] s = line.Split(new char[]{'\t'});
                        Armor arm = new Armor(s[0], int.Parse(s[1]), int.Parse(s[2]));
                        db.armors.Add(arm.Name, arm);
                    }
                }
            } while (line != null);
        }
        #endregion
        #region Weapons
        using (StreamReader reader = new StreamReader(path + WPN_PATH)) {
            do {
                line = reader.ReadLine();

                if (line != null) {
                    if (!line.Contains("wclass")) {
                        string[] s = line.Split(new char[] { '\t' });
                        Weapon wpn = new Weapon(s[0],int.Parse(s[1]),int.Parse(s[2]),s[3]);
                        db.weapons.Add(wpn.Name, wpn);
                    }
                }
            } while (line != null);
        }
        #endregion
        #region Monstats
        using (StreamReader reader = new StreamReader(path + MON_PATH)) {
            for (; ;) {
                line = reader.ReadLine();

                if (line != null) {
                    string[] s = line.Split(new char[] { '\t' });
                    if (s[1].Length == 0 && s[2].Length == 0 && s[3].Length == 0) db.monEntries = int.Parse(s[0]);
                    else {
                        if (!line.Contains("TreasureClass")) {
                            MonStat mon = new MonStat(s[0], s[1], int.Parse(s[2]), s[3]);
                            db.monstats.Add(mon.Class,mon);
                        }
                    }
                } else break;
            }            
        }
        #endregion
        #region Treasure Classes
        using (StreamReader reader = new StreamReader(path + TCE_PATH)) {
            for (; ;) {
                line = reader.ReadLine();

                if (line != null) {
                    if (!line.Contains("Treasure Class")) {
                        string[] s = line.Split(new char[] { '\t' });
                        TreasureClass tc = new TreasureClass(s[0], s[1], s[2], s[3]);
                        db.tcs.Add(tc.Name,tc);
                    }
                } else break;
            }
        }
        #endregion
        #region Prefixes
        using (StreamReader reader = new StreamReader(path + PRF_PATH)) {
            for (; ;) {
                line = reader.ReadLine();

                if (line != null) {
                    string[] s = line.Split(new char[] { '\t' });
                    if (s[1].Length == 0 && s[2].Length == 0 && s[3].Length == 0) db.prefixEntries = int.Parse(line);
                    else {
                        if (!line.Contains("mod1code")) {
                            NonGeneratedPrefix pref = new NonGeneratedPrefix(s[0], s[1], int.Parse(s[2]), int.Parse(s[3]));
                            db.prefixes.Add(pref);
                        }
                    }
                } else break;
            }
        }
        #endregion
        #region Suffixes
        using (StreamReader reader = new StreamReader(path + SUF_PATH)) {
            for (; ;) {
                line = reader.ReadLine();

                if (line != null) {
                    string[] s = line.Split(new char[] { '\t' });
                    if (s[1].Length == 0 && s[2].Length == 0 && s[3].Length == 0) db.suffixEntries = int.Parse(line);
                    else {
                        if (!line.Contains("mod1code")) {
                            NonGeneratedSuffix suff = new NonGeneratedSuffix(s[0], s[1], int.Parse(s[2]), int.Parse(s[3]));
                            db.suffixes.Add(suff);
                        }
                    }
                } else break;
            }
        }
        #endregion

        return db;
    }

}
