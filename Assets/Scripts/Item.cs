﻿using UnityEngine;
using System.Collections;

public class Item : IItem{



    string m_name;
    public string Name {
        get { return m_name; }
        private set { m_name = value; }
    }

    int[] m_stats;
    public int[] BaseStats {
        get { return m_stats; }
        private set { m_stats = value; }
    }

    Prefix m_prefix;
    public Prefix prefix {
        get { return m_prefix; }
        private set { m_prefix = value; }
    }

    Suffix m_suffix;
    public Suffix suffix {
        get { return m_suffix; }
        private set { m_suffix = value; }
    }

    EquipmentType m_type;
    public EquipmentType Type {
        get { return m_type; }
        private set { m_type = value; }
    }


    public Item(EquipmentType type, string name, int[] baseStats, Prefix p, Suffix s) {
        this.Type = type;
        this.Name = name;
        this.BaseStats = baseStats;
        this.prefix = p;
        this.suffix = s;
    }

    public override string ToString() {
        DataBase db = DBHelper.Load();
        string res = "";
        string prefixName = "";
        string suffixName = "";
        string prefixBonus = "";
        string suffixBonus = "";
        string sign = "+";
        if (prefix != null) {
            prefixName = prefix.Name+" ";
            sign = (prefix.Value > 0) ? "+" : "-";
            prefixBonus = prefix.isProcValue? string.Format("{0}{1}% {2}\n", sign, prefix.Value, prefix.Code):string.Format("{0}{1} {2}\n", sign, prefix.Value, prefix.Code);
        }
        if (suffix != null) {
            suffixName = " "+suffix.Name;
            sign = (suffix.Value > 0) ? "+" : "-";
            suffixBonus = suffix.isProcValue ? string.Format("{0}{1}% {2}", sign, suffix.Value, suffix.Code) : string.Format("{0}{1} {2}", sign, suffix.Value, suffix.Code);
            
        }
        res += string.Format("{0}{1}{2}\n", prefixName, Name.Replace("_", " "), suffixName);
        switch (Type) {
            case EquipmentType.Armor:
                res += string.Format("Defense: {0}\n", BaseStats[0]);
                break;
            case EquipmentType.Weapon:
                res += (DBHelper.oneHandedWpn.Contains(db.weapons[Name].Class)) ? string.Format("One-handed Damage: {0} - {1}\n", BaseStats[0], BaseStats[1]) : string.Format("Two-handed Damage: {0} - {1}\n", BaseStats[0], BaseStats[1]);
                break;
        }
        res += prefixBonus;
        res += suffixBonus;
        
        return res;
    }
}
